const std = @import("std");
const HttpServer = @import("zHttpServer");
const IEndpoint = HttpServer.IEndpoint;
const StreamServer = std.net.StreamServer;
const Self = @This();
const Util = @import("Util.zig");
const UserRow = Util.UserRow;

endpoint: IEndpoint = .{ .name = "StaticFile", .acceptFn = accept },
db: @import("Database.zig"),

const HTML =
    \\<!DOCTYPE html><html><body>
    \\<form action="/logout" method="post">
    \\<input type="submit" value="Logout">
    \\</form>
    \\<h1> Hello {s}</h1>
    \\<h1> Email: {s}</h1>
    \\</body></html>
;

const TEMPLATE = HttpServer.TEMPLATE_RESPONSE ++ HTML;

pub fn termSlice(b: []const u8) []const u8 {
    return b[0 .. HttpServer.Strings.find(&[_]u8{0}, b) orelse b.len];
}

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    var req = ctx.req;
    var client = ctx.client;
    var cookie = req.get_cookie("cookie") orelse {
        client.stream.close();
        return true;
    };
    var self = @fieldParentPtr(Self, "endpoint", endpoint);
    var user = self.db.get_user_from_cookie(cookie) catch |err| switch (err) {
        error.CookieWithInvalidUser => {
            const REMOVECOOKIE = comptime Util.RedirectHome
                .add_field("Set-Cookie", "cookie=deleted; Max-Age=-1;")
                .end();
            client.stream.writer().print(REMOVECOOKIE, .{}) catch {
                client.stream.close();
                return true;
            };
            ctx.http_server.reaccept(client.*) catch client.stream.close();
            return true;
        },
        else => {
            std.debug.print("{any} | Greet\n", .{err});
            return true;
        },
    };

    client.stream.writer().print(TEMPLATE, .{ "200 OK", "text/html", std.fmt.count(HTML, .{ termSlice(&user.username), termSlice(&user.email) }), termSlice(&user.username), termSlice(&user.email) }) catch {
        client.stream.close();
        return true;
    };
    ctx.http_server.reaccept(client.*) catch client.stream.close();
    return true;
}
