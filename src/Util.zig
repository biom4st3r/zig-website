const std = @import("std");
const HttpServer = @import("zHttpServer");
const sqlite = @import("sqlite");
const IEndpoint = HttpServer.IEndpoint;
const StreamServer = std.net.StreamServer;
const Strings = HttpServer.DataTypes.Strings;
const Sha3 = std.crypto.hash.sha3.Sha3_512;
const Db = @import("Database.zig");

pub const BaseRes = HttpServer.Response
    .add_code("HTTP/1.1", "302 Found")
    .add_field("Server", "zhttp-server")
    .connection(.@"keep-alive")
    .add_field("Content-Length", "0");

pub const RedirectHome = BaseRes.add_field("Location", "/");

pub fn sanitize(comptime T: type, obj: *T) void {
    inline for (std.meta.fields(T)) |field| {
        std.crypto.utils.secureZero(u8, &@field(obj, field.name));
    }
}

pub fn hash(vals: anytype) [Sha3.digest_length]u8 {
    var h = Sha3.init(.{});

    inline for (vals) |val| {
        h.update(val);
    }
    var b: [Sha3.digest_length]u8 = undefined;
    h.final(&b);
    return b;
}

pub fn rand_bytes(comptime bytes: usize) [bytes]u8 {
    var b: [bytes]u8 = undefined;
    std.crypto.random.bytes(&b);
    return b;
}

pub const UserRow = struct {
    pub const email_len = 65;
    pub const username_len = 31;
    pub const EmailType = [email_len:0]u8;
    pub const UsernameType = [username_len:0]u8;
    pub const PasswordType = [Sha3.digest_length]u8;
    pub const SaltType = [24]u8;
    email: EmailType,
    username: UsernameType,
    password: PasswordType,
    salt: SaltType,
    pub fn sanitize(self: *UserRow) void {
        inline for (std.meta.fields(UserRow)) |field| {
            std.crypto.utils.secureZero(u8, &@field(self, field.name));
        }
    }
    pub fn init(self: *UserRow, email: []const u8, username: []const u8, password: []const u8) void {
        self.sanitize();
        self.salt = rand_bytes(24);
        @memcpy(self.email[0..email.len], email);
        // self.email[email.len] = 0;

        @memcpy(self.username[0..username.len], username);
        // self.username[username.len] = 0;
        self.password = hash(.{ password, &self.salt });
        // var hash = Sha3.init(.{});
        // hash.update(password);
        // hash.update(&self.salt);
        // hash.final(&self.password);
    }

    pub fn insert(self: *const UserRow, db: *Db) !void {
        try db.create_user(self);
    }

    pub fn exists(self: *const UserRow, db: *Db) !bool {
        return try db.user_exists(&self.username, &self.email);
    }

    pub fn print(self: *UserRow) void {
        HttpServer.log_print(.All, "|{s}|{s}|{s}|{s}|\n", .{ &self.email, &self.username, &self.password, &self.salt });
    }
};
