const std = @import("std");
const HttpServer = @import("zHttpServer");
const sqlite = @import("sqlite");
const IEndpoint = HttpServer.IEndpoint;
const StreamServer = std.net.StreamServer;
const Strings = HttpServer.DataTypes.Strings;
const Sha3 = std.crypto.hash.sha3.Sha3_512;
const Util = @import("Util.zig");
const UserRow = Util.UserRow;
const sanitize = @import("Util.zig").sanitize;
const Db = @import("Database.zig");

const TEMPLATE = HttpServer.DataTypes.ResponseBuilder
    .add_code("HTTP/1.1", "302 Found")
    .add_field("Server", "zhttp-server")
    .content_type("text/plain")
    .add_field("Content-Length", "0")
    .add_field("Location", "{s}")
    .add_field("Set-Cookie", "cookie={s}; Max-Age={d}")
    .connection(.@"keep-alive")
    .end();

endpoint: IEndpoint = .{ .name = "LoggedInPageEP", .acceptFn = accept },

db: Db,
// const query_check_for_user = "SELECT password,salt FROM users WHERE username LIKE ?";
pub fn accepts(endpoint: *IEndpoint, req: *HttpServer.DataTypes.Request) bool {
    _ = endpoint;

    return req.method == .POST and std.ascii.eqlIgnoreCase(req.path, "/attemptlogin");
}

pub fn accept_err(endpoint: *IEndpoint, req: *HttpServer.DataTypes.Request, client: *StreamServer.Connection) !void {
    var self = @fieldParentPtr(@This(), "endpoint", endpoint);

    var data = @constCast(req.full_request[HttpServer.DataTypes.Strings.find("\r\n\r\n", req.full_request) orelse {
        return error.OtherError;
    } + 4 ..]);

    data = HttpServer.DataTypes.Request.url_decode_mut(data) catch {
        return error.OtherError;
    };

    const USER_KEY = "username=";
    const ustart = Strings.find(USER_KEY, data) orelse return error.BadData;
    const uend = Strings.find("&", data) orelse return error.BadData;
    var username = data[ustart + USER_KEY.len .. uend];

    data = data[uend + 1 ..];
    const PASS_KEY = "password=";
    const pstart = Strings.find(PASS_KEY, data) orelse return error.BadData;
    var password = data[pstart + PASS_KEY.len ..];
    defer std.crypto.utils.secureZero(u8, password);
    defer std.crypto.utils.secureZero(u8, username);

    if (self.db.get_userid_if_valid_password(username, password)) |user_id| {
        std.debug.print("Success\n", .{});
        const cookie = self.db.insert_cookie(user_id) catch {
            return error.DbError;
        };
        client.stream.writer().print(TEMPLATE, .{ "/", cookie, Db.MAX_COOKIE_AGE }) catch {
            return error.StreamError;
        };
    } else {
        return error.OtherError;
    }
    return;
}

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    var self = @fieldParentPtr(@This(), "endpoint", endpoint);
    _ = self;
    var req = ctx.req;
    var client = ctx.client;
    accept_err(endpoint, req, client) catch |err| switch (err) {
        error.BadData => {
            _ = client.stream.write(comptime Util.RedirectHome.end()) catch {
                client.stream.close();
                return true;
            };
        },
        error.StreamError => {
            client.stream.close();
            return true;
        },
        else => {
            const REMOVECOOKIE = comptime Util
                .RedirectHome
                .add_field("Set-Cookie", "cookie=deleted; Max-Age=-1;")
                .end();
            client.stream.writer().print(REMOVECOOKIE, .{}) catch {
                client.stream.close();
            };
            ctx.http_server.reaccept(client.*) catch {};
            return true;
        },
    };
    ctx.http_server.reaccept(client.*) catch client.stream.close();
    return true;
}
