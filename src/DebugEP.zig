const std = @import("std");
const HttpServer = @import("zHttpServer");
const IEndpoint = HttpServer.IEndpoint;
const StreamServer = std.net.StreamServer;

endpoint: IEndpoint = .{ .name = "StaticFile", .acceptFn = accept },

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    _ = endpoint;
    ctx.req.print();
    // var self = @fieldParentPtr(Self, "endpoint", endpoint);
    // var req = ctx.req;
    // var client = ctx.client;
    // if (!self.accepts(endpoint, req)) return false;
    return false;
}
