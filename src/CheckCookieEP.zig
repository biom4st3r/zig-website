const std = @import("std");
const HttpServer = @import("zHttpServer");
const IEndpoint = HttpServer.IEndpoint;
const StreamServer = std.net.StreamServer;
const Self = @This();
const Strings = HttpServer.DataTypes.Strings;

pub fn get_cookie(self: *HttpServer.DataTypes.Request, name: []const u8) ?[]const u8 {
    var cookies = self.get_field_value("\nCookie: ", "\r\n") orelse return null;
    var iter = std.mem.splitSequence(u8, cookies, "; ");
    while (iter.next()) |next| {
        if (Strings.find("=", next)) |idx| {
            if (!Strings.eql(next[0..idx], name)) continue;
            return next[idx + 1 ..];
        }
    }
    return null;
}
const TEMPLATE_REDIRECT_RESPONSE = HttpServer.DataTypes.ResponseBuilder
    .add_code("HTTP/1.1", "301 Moved Permanently")
    .add_field("Server", "zhttp-server")
    .add_field("Content-Length", "0")
    .add_field("Location", "{s}")
    .connection(.@"keep-alive")
    .add_field("Set-Cookie", "cookie=deleted; Max-Age=-1")
    .add_field("Accept-Ranges", "bytes")
    .end();

pub fn send_redirect_response(client: *StreamServer.Connection, destination: []const u8) !void {
    var buf: [512]u8 = undefined;
    var data = try std.fmt.bufPrint(&buf, TEMPLATE_REDIRECT_RESPONSE, .{destination});
    _ = try client.stream.write(data);
}

endpoint: IEndpoint = .{ .name = "CheckCookie", .acceptFn = accept },
db: @import("Database.zig"),

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    var self = @fieldParentPtr(Self, "endpoint", endpoint);
    var req = ctx.req;
    var client = ctx.client;
    const cookie = get_cookie(req, "cookie") orelse "";
    var valid_cookie = self.db.verify_cookie(cookie) catch false;
    if (valid_cookie) return false;

    self.db.remove_cookie(cookie) catch {

        //  I want to send a redirect anyways, so just skip the error
    };

    send_redirect_response(client, "/") catch {
        client.stream.close();
        return true;
    };

    ctx.http_server.reaccept(client.*) catch {
        client.stream.close();
        return true;
    };
    return true;
}
