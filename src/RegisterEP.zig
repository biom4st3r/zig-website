const std = @import("std");
const HttpServer = @import("zHttpServer");
const sqlite = @import("sqlite");
const IEndpoint = HttpServer.IEndpoint;
const StreamServer = std.net.StreamServer;
const Strings = HttpServer.DataTypes.Strings;
const Sha3 = std.crypto.hash.sha3.Sha3_512;
const Util = @import("Util.zig");
const UserRow = @import("Util.zig").UserRow;
const Db = @import("Database.zig");

const TEMPLATE = HttpServer.DataTypes.ResponseBuilder
    .add_code("HTTP/1.1", "302 Found")
    .add_field("Server", "zhttp-server")
    .content_type("text/plain")
    .add_field("Content-Length", "0")
    .add_field("Location", "{s}")
    .end();

endpoint: IEndpoint = .{ .name = "CreateUserEP", .acceptFn = accept },
db: Db,

pub fn accept_err(endpoint: *IEndpoint, req: *HttpServer.DataTypes.Request, client: *StreamServer.Connection) !void {
    // https://github.com/vrischmann/zig-sqlite/tree/f00fd245c52a56219ce3e2a559d88ed0858c983e
    var self = @fieldParentPtr(@This(), "endpoint", endpoint);
    var data = @constCast(req.full_request[HttpServer.DataTypes.Strings.find("\r\n\r\n", req.full_request) orelse {
        return error.NoData;
        // client.stream.close();
        // return;
    } + 4 ..]);
    var o_data = data;
    defer std.crypto.utils.secureZero(u8, o_data);
    // TODO Maybe use content_length to resize data

    // const content_length = std.fmt.parseInt(usize, req.get_field_value("\nContent-Length: ", "\r\n") orelse {
    //     client.stream.close();
    //     return;
    // }, 0) catch {
    //     client.stream.close();
    //     return;
    // };
    // data = data[0..content_length];
    data = HttpServer.DataTypes.Request.url_decode_mut(data) catch {
        return error.FailedPayloadDecode;
    };

    var split = Strings.find("&", data) orelse {
        return error.FailedPayloadParse;
    };
    const EMAIL_KEY = "email=";
    const i_email = Strings.find(EMAIL_KEY, data) orelse {
        return error.FailedPayloadParse;
    };
    var email = data[i_email + EMAIL_KEY.len .. split];
    if (email.len > Util.UserRow.email_len) {
        return error.EmailToLong;
    }

    data = data[split + 1 ..];

    split = Strings.find("&", data) orelse {
        return error.FailedPayloadParse;
    };
    const USER_KEY = "username=";
    const i_user = Strings.find(USER_KEY, data) orelse {
        return error.FailedPayloadParse;
    };
    var username = data[i_user + USER_KEY.len .. split];
    if (username.len > Util.UserRow.username_len) {
        return error.UsernameToLong;
    }
    data = data[split + 1 ..];

    const PASS_KEY = "password=";
    const i_pass = Strings.find(PASS_KEY, data) orelse {
        return error.FailedPayloadParse;
    };
    var password = data[i_pass + PASS_KEY.len ..];

    // TODO Check length. There could be anything after the password
    // TODO Decode the individual parts of the payload instead of the whole payload
    //https://github.com/vrischmann/zig-sqlite#reading-data
    // TODO Should probably reuse this.
    var new_row: UserRow = undefined;

    new_row.init(email, username, password);
    defer new_row.sanitize();
    new_row.print();
    if (try new_row.exists(&self.db)) {
        return error.CredsExist;
    }
    try new_row.insert(&self.db);

    try client.stream.writer().print(TEMPLATE, .{"/"});
}

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    var self = @fieldParentPtr(@This(), "endpoint", endpoint);

    var req = ctx.req;
    var client = ctx.client;
    if (req.method == .POST and std.ascii.eqlIgnoreCase(req.path, "/createuser")) {} else {
        return false;
    }
    _ = self;
    var err_msg: ?[]const u8 = null;
    accept_err(endpoint, req, client) catch |err| switch (err) {
        error.UsernameToLong => {
            err_msg = "UsernameToLong";
        },
        error.EmailToLong => {
            err_msg = "EmailToLong";
        },
        error.CredsExist => {
            err_msg = "EmailOrUsernameTaken";
        },
        else => |errr| {
            std.debug.print("RegisterEP: {any}\n", .{errr});
            client.stream.close();
            return true;
        },
    };
    if (err_msg) |msg| {
        const REDIRECT = comptime Util.BaseRes.add_field("Location", "/register?msg={s}").end();

        client.stream.writer().print(REDIRECT, .{msg}) catch {
            client.stream.close();
            return true;
        };
    }
    ctx.http_server.reaccept(client.*) catch {
        client.stream.close();
    };
    return true;
}
