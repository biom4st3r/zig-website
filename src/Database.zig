const std = @import("std");
const HttpServer = @import("zHttpServer");
const sqlite = @import("sqlite");
const Strings = HttpServer.DataTypes.Strings;
const UserRow = @import("Util.zig").UserRow;
const sanitize = @import("Util.zig").sanitize;
const Util = @import("Util.zig");
const Sha3 = std.crypto.hash.sha3.Sha3_512;

db: sqlite.Db,
pub fn user_exists(self: *@This(), username: ?[]const u8, email: ?[]const u8) !bool {
    const IdStruct = struct { id: u64 };
    const query_check_for_user = "SELECT id FROM users WHERE ";
    const with_email = "email LIKE ?";
    const with_username = "username LIKE ?";
    const OR = " OR ";

    if (username) |u| {
        if (email) |e| {
            var stmt = try self.db.prepare(query_check_for_user ++ with_email ++ OR ++ with_username);
            defer stmt.deinit();

            var row: ?IdStruct = try stmt.one(IdStruct, .{}, .{ .email = e, .username = u });
            return row != null;
        } else {
            var stmt = try self.db.prepare(query_check_for_user ++ with_username);
            defer stmt.deinit();

            var row: ?IdStruct = try stmt.one(IdStruct, .{}, .{ .username = u });
            return row != null;
        }
    }
    if (email) |e| {
        var stmt = try self.db.prepare(query_check_for_user ++ with_email);
        defer stmt.deinit();

        var row: ?IdStruct = try stmt.one(IdStruct, .{}, .{ .email = e });
        return row != null;
    }
    return false;
}

pub fn get_userid_if_valid_password(self: *@This(), username: []const u8, password: []const u8) ?u64 {
    const query_check_for_user = "SELECT id,password,salt FROM users WHERE username LIKE ?";
    var stmt = self.db.prepare(query_check_for_user) catch return null;
    defer stmt.deinit();

    var row = (stmt.one(struct { user_id: usize, password: UserRow.PasswordType, salt: UserRow.SaltType }, .{}, .{ .username = username }) catch return null) orelse return null;

    var hash = Sha3.init(.{});
    hash.update(password);
    hash.update(&row.salt);
    // std.crypto.pwhash.bcrypt;
    var buff: [Sha3.digest_length]u8 = [_]u8{0} ** Sha3.digest_length;
    hash.final(&buff);

    return if (Strings.eql(&row.password, &buff)) row.user_id else null;
}

pub const MAX_COOKIE_AGE = 60 * 60 * 24; //60 * 60 * 24 * 15; // 15 Days
pub fn verify_cookie(self: *@This(), cookie: []const u8) !bool {
    // TODO Batch this with deleting
    const query_check_for_cookie = "SELECT user_id,strftime('%s', created) AS unixepoch FROM cookies WHERE cookie LIKE ?";
    var stmt = try self.db.prepare(query_check_for_cookie);
    defer stmt.deinit();
    // TODO cache cookies for reuse
    var data = try stmt.one(struct { user_id: u64, created: i64 }, .{}, .{cookie}) orelse return false;
    if (std.time.timestamp() - data.created > MAX_COOKIE_AGE) {
        return false; // self.remove_cookie?
    }
    return true;
}

const NameEmail = struct { username: [31:0]u8, email: [65:0]u8 };
pub fn get_user_from_cookie(self: *@This(), cookie: []const u8) !NameEmail {
    const query = "SELECT users.username, users.email FROM users JOIN cookies ON users.id = cookies.user_id WHERE cookies.cookie LIKE ?";
    var stmt = try self.db.prepare(query);
    defer stmt.deinit();

    var row = try stmt.one(NameEmail, .{}, .{cookie}) orelse return error.CookieWithInvalidUser;

    std.debug.print("un:{any} em:{any} {any}\n", .{ &row.username, &row.email, row.username });

    return row;
}

pub fn remove_cookie(self: *@This(), cookie: []const u8) !void {
    const query_delete_cookie = "DELETE FROM cookies WHERE cookie LIKE ?";
    var stmt = try self.db.prepare(query_delete_cookie);
    defer stmt.deinit();
    try stmt.exec(.{}, .{cookie});
}

const base64 = std.base64.standard.Encoder;
const cookie_bytes = 256 / 8;

pub fn insert_cookie(self: *@This(), user_id: u64) ![base64.calcSize(cookie_bytes)]u8 {
    const query_insert_cookie = "INSERT INTO cookies (cookie, user_id) VALUES (?,?)";

    var cookie_str: [base64.calcSize(cookie_bytes)]u8 = undefined;
    var out = base64.encode(&cookie_str, &Util.rand_bytes(cookie_bytes));
    _ = out;
    var stmt = try self.db.prepare(query_insert_cookie);
    defer stmt.deinit();

    try stmt.exec(.{}, .{
        cookie_str,
        user_id,
    });
    return cookie_str;
}

pub fn sliceSentinel(arg: anytype) []const u8 {
    return arg[0..std.mem.indexOfSentinel(u8, 0, arg)];
}

pub fn create_user(self: *@This(), row: *const UserRow) !void {
    const query_insert_user = "INSERT INTO users (email,username,password,salt) VALUES (?,?,?,?)";
    var create = try self.db.prepare(query_insert_user);
    defer create.deinit();
    try create.exec(.{}, .{ sliceSentinel(&row.email), sliceSentinel(&row.username), row.password, row.salt });
}
