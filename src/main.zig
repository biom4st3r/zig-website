const std = @import("std");
const HttpServer = @import("zHttpServer");
const sqlite = @import("sqlite");
const IEndpoint = HttpServer.IEndpoint;
const Database = @import("Database.zig");
const StreamServer = std.net.StreamServer;

const CREATE_USERS =
    \\CREATE TABLE IF NOT EXISTS users(
    \\id INTEGER PRIMARY KEY,
    \\email TEXT,
    \\username TEXT,
    \\password BLOB,
    \\salt BLOB,
    \\);
;
const CREATE_COOKIES =
    \\CREATE TABLE IF NOT EXISTS cookies(
    \\id INTEGER PRIMARY KEY,
    \\cookie TEXT,
    \\user_id INTEGER,
    \\FOREIGN KEY(user_id) REFERENCES users(id)  
    \\);
;
const Strings = HttpServer.DataTypes.Strings;
pub fn get_cookie(self: *HttpServer.DataTypes.Request, name: []const u8) ?[]const u8 {
    var cookies = self.get_field_value("\nCookie: ", "\r\n") orelse return null;
    var iter = std.mem.splitSequence(u8, cookies, "; ");
    while (iter.next()) |next| {
        if (Strings.find("=", next)) |idx| {
            if (!Strings.eql(next[0..idx], name)) continue;
            return next[idx + 1 ..];
        }
    }
    return null;
}

pub fn init_db() !void {
    var stmt0 = try db.prepare(CREATE_USERS);
    defer stmt0.deinit();

    var stmt1 = try db.prepare(CREATE_COOKIES);
    defer stmt1.deinit();
    try stmt0.exec(.{}, .{});
    try stmt1.exec(.{}, .{});
}

var db: sqlite.Db = undefined;
pub fn main() !void {
    db = try sqlite.Db.init(.{
        .mode = sqlite.Db.Mode{ .File = "./server.db" },
        .open_flags = .{
            .write = true,
            .create = true,
        },
        .threading_mode = .MultiThread,
    });
    // try init_db();
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var allocator = gpa.allocator();
    var config: HttpServer.HTTP_CONFIG = HttpServer.Args.parse(allocator);
    var server = HttpServer{};
    server.init(allocator, config);

    HttpServer.LOGGING = .None;
    var database = Database{ .db = db };

    var debugep = @import("DebugEP.zig"){};
    server.add_endpoint(&debugep.endpoint);

    var nested = HttpServer.Endpoints.NestedEP.init(allocator);
    // .init(allocator);
    var cookieCheck = @import("CheckCookieEP.zig"){ .db = .{ .db = db } };
    nested.register(&cookieCheck.endpoint);

    var logout = IEndpoint{
        .name = "Logout",
        .acceptFn = struct {
            const REDIRECT = HttpServer.HTTP11_TEMPLATE
                .add_field("Content-Length", "0")
                .add_field("Location", "{s}")
                .add_field("Set-Cookie", "cookie=deleted; Max-Age: -1")
                .end();
            pub fn accept(self: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
                _ = self;
                var req = ctx.req;
                var client = ctx.client;
                if (HttpServer.Strings.eql("/logout", req.path) and req.method == .POST) {} else {
                    return false;
                }
                var datab: Database = .{ .db = db };
                if (req.get_cookie("cookie")) |cookie| {
                    datab.remove_cookie(cookie) catch {};
                }
                client.stream.writer().print(REDIRECT, .{ "302 Found", "/" }) catch {
                    client.stream.close();
                    return true;
                };
                ctx.http_server.reaccept(client.*) catch client.stream.close();
                return true;
            }
        }.accept,
    };

    nested.register(&logout); // var valid = @import("StaticFile.zig"){
    //     .html = "<body>Success</body>",
    //     .path = "/",
    // };
    // nested.register(&valid.endpoint);
    var greet = @import("GreetingEP.zig"){ .db = database };
    nested.register(&greet.endpoint);

    var has_session_cookie = HttpServer.Endpoints.WrappedEP{
        .wrapped = &nested.endpoint,
        .acceptsFn = struct {
            pub fn accepts(endpoint: *IEndpoint, req: *HttpServer.DataTypes.Request) bool {
                _ = endpoint;
                var cookie = get_cookie(req, "cookie") orelse return false;
                _ = cookie;
                return true;
            }
        }.accepts,
    };
    server.add_endpoint(&has_session_cookie.endpoint);

    var homepageEP = @import("StaticFile.zig"){
        .html = &HttpServer.HtmlMinifier(@embedFile("index.html"), false).val,
        .path = "/",
    };
    server.add_endpoint(&homepageEP.endpoint);

    var registerEP = @import("StaticFile.zig"){
        .html = &HttpServer.HtmlMinifier(@embedFile("Register.html"), false).val,
        .path = "/register",
    };
    server.add_endpoint(&registerEP.endpoint);

    var createUserEP = @import("RegisterEP.zig"){ .db = database };
    server.add_endpoint(&createUserEP.endpoint);

    var loggedInEP = @import("LoginEP.zig"){ .db = database };
    server.add_endpoint(&loggedInEP.endpoint);
    try server.start();
}
