const std = @import("std");
const HttpServer = @import("zHttpServer");
const IEndpoint = HttpServer.IEndpoint;
const StreamServer = std.net.StreamServer;
const Self = @This();

pub const TEMPLATE = &HttpServer.HtmlMinifier(@embedFile("Login.html"), false).val;
endpoint: IEndpoint = .{ .name = "StaticFile", .acceptFn = accept },
html: []const u8,
path: []const u8,

pub fn accept(endpoint: *IEndpoint, ctx: *IEndpoint.Ctx) bool {
    var self = @fieldParentPtr(Self, "endpoint", endpoint);
    var req = ctx.req;
    var client = ctx.client;
    if (!HttpServer.DataTypes.Strings.eql(self.path, req.path)) {
        return false;
    }
    HttpServer.send_simple_response(client, "200 OK", "text/html", self.html) catch {
        client.stream.close();
        return true;
    };
    ctx.http_server.reaccept(client.*) catch client.stream.close();
    return true;
}
